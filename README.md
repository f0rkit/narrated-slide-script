# Narrated slide script

This script extracts each page from a pdf, and then joins pictures and audio recordings to generate a full-hd compressed video (i.e., a narrated presentation)

Requirements:
ffmpeg and ImageMagick

Input:
Please note that both image and audio files should be numbered in ascending order and the file relative to each slide should share the same name (except the file extension).

Input file example:
#slide 1
1.png # slide1
1.mp3 # audio registration relative to slide1


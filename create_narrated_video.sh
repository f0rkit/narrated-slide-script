#!/bin/bash
echo "This script extracts each page from a pdf, and then joins pictures and audio recordings to generate a full-hd compressed video (i.e., a narrated presentation)"
slide_ext="png"
echo "Slide extension set to $slide_ext"

audio_ext="mp3"
echo "Audio extension set to $audio_ext"

number_of_slides=3
echo "Number of slides set to $number_of_slides"

input_filename="doc"
output_filename="presentation"

convert -density 500 $input_filename.pdf -scale 1920x1080 %d.png

for i in $(seq 0 $number_of_slides)	
do
   echo "Processing $i-th file"
   ffmpeg -loop 1 -i $i.$slide_ext -i $i.$audio_ext -shortest -acodec copy -c:v libx264 -crf 0 -preset ultrafast -c:a libmp3lame -b:a 320k $i.mp4
   ffmpeg -i $i.mp4 -q:v 1 $i.mpg
done

echo "Joining $number_of_slides files"

string=""
for l in $(seq 0 $number_of_slides)
do
	string="$string $l.mpg"
done

cat $string | ffmpeg -f mpeg -i - -crf 23 -preset slow -b:a 96k $output_filename.mp4